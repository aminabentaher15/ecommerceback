﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace ECommerce.Models
{
    public class Commande
    {
        // Propriétés de la classe Commande
        public Guid CommandeId { get; set; }
        public DateTime DateCommande { get; set; }
        public string? Libelle { get; set; }

        // Relation avec la classe Client : Une commande est associée à un client
        public Guid ClientId { get; set; }
        [JsonIgnore]
        public Client? Client { get; set; }

        // Relation avec la classe Produit : Une commande peut contenir plusieurs produits
        [JsonIgnore]
        public virtual List<CommandeProduit>? CommandeProduits { get; set; }= new List<CommandeProduit>();
    }
}
