﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace ECommerce.Models
{
    public class Client
    {
        // Propriétés de la classe Client
        public Guid ClientId { get; set; }
        public string? Nom { get; set; }
        public string? Email { get; set; }
        public string? Adresse { get; set; }

        // Relation avec la classe Commande : Un client peut avoir plusieurs commandes
        [JsonIgnore]
        public List<Commande> Commandes { get; set; } = new List<Commande>();

    }
}
