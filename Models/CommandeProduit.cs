﻿using System.Text.Json.Serialization;

namespace ECommerce.Models
{
    public class CommandeProduit
    {
        public Guid CommandeProduitId { get; set; }
        // Identifiant de la commande
        public Guid CommandeId { get; set; }

        // Identifiant du produit
        public Guid ProduitId { get; set; }
        public float quantite { get; set; }

        // Navigation vers la commande correspondante

        public Commande? Commande { get; set; }

        // Navigation vers le produit correspondant

        public Produit? Produit { get; set; }
    }
}
