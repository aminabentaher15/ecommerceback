﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace ECommerce.Models
{
    public class Produit
    {
        // Propriétés de la classe Produit
        public Guid ProduitId { get; set; }
        public string Nom { get; set; }
        public string Description { get; set; }
        public int Prix { get; set; }

        // Relation avec la classe Commande : Un produit peut être inclus dans plusieurs commandes
        [JsonIgnore]
        public virtual List<CommandeProduit> CommandeProduits { get; set; } = new List<CommandeProduit>();

    }
}
