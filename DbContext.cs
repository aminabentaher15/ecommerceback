﻿using Microsoft.EntityFrameworkCore;
using ECommerce.Models;
using System.Collections.Generic;
using System.Reflection.Emit;

namespace ECommerce
{
    public class ECommerceDbContext : DbContext
    {
        // Déclaration des DbSet pour chaque classe de modèle
        public DbSet<Client> Clients { get; set; }
        public DbSet<Commande> Commandes { get; set; }
        public DbSet<Produit> Produits { get; set; }
        public DbSet<CommandeProduit> CommandeProduits { get; set; }
        // Constructeur pour initialiser DbContext avec les options
        public ECommerceDbContext(DbContextOptions<ECommerceDbContext> options) : base(options)
        {
        }

        // Méthode pour configurer le modèle de données
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Configuration de l'identifiant principal et l'auto-incrémentation pour chaque table
            modelBuilder.Entity<Client>().HasKey(c => c.ClientId);
            modelBuilder.Entity<Commande>().HasKey(c => c.CommandeId);
            modelBuilder.Entity<Produit>().HasKey(p => p.ProduitId);
            modelBuilder.Entity<CommandeProduit>().HasKey(p => p.CommandeProduitId);

            // Exemple de configuration de clé étrangère
            modelBuilder.Entity<Commande>()
                .HasOne(c => c.Client)
                .WithMany(cl => cl.Commandes)
                .HasForeignKey(c => c.ClientId);


            modelBuilder.Entity<CommandeProduit>()
               .HasOne(cp => cp.Commande)
               .WithMany(c => c.CommandeProduits)
               .HasForeignKey(cp => cp.CommandeId);

            modelBuilder.Entity<CommandeProduit>()
                .HasOne(cp => cp.Produit)
                .WithMany(p => p.CommandeProduits)
                .HasForeignKey(cp => cp.ProduitId);
        }
    }
}
