﻿using AutoMapper;
using ECommerce.DTOs;
using ECommerce.Models;

namespace ECommerce.Profiles
{
    public class LigneCommandeProfile : Profile
    {
        public LigneCommandeProfile()
        {
            CreateMap<LigneCommandeDto, CommandeProduit>();
            CreateMap<CommandeProduit, LigneCommandeDto>()
                .ForMember(dest => dest.NomProduit, opt => opt.MapFrom(src =>
                    src.Produit != null ? src.Produit.Nom : null))
                .ForMember(dest => dest.LibelleCommande, opt => opt.MapFrom(src =>
                    src.Commande != null ? src.Commande.Libelle : null))
                .ForMember(dest => dest.NomClient, opt => opt.MapFrom(src =>
                    src.Commande.Client.Nom != null ? src.Commande.Client.Nom : null));
           
        }
    }
}
