﻿using ECommerce.DTOs;
using ECommerce.Models;
using AutoMapper;

namespace ECommerce.Profiles
{
    public class CommandeProfile : Profile
    {
        public CommandeProfile()
        {
            CreateMap<CommandeDto, Commande>();
            CreateMap<Commande, CommandeDto>()
                .ForMember(dest => dest.NomProduit, opt => opt.MapFrom(src =>
                    src.CommandeProduits.FirstOrDefault() != null ? src.CommandeProduits.FirstOrDefault().Produit.Nom : null))
                .ForMember(dest => dest.NomClient, opt => opt.MapFrom(src =>
                    src.Client != null ? src.Client.Nom : null));
        }
    }
}
