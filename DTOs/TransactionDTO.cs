﻿namespace ECommerce.DTOs
{
    public class TransactionDTO
    {
        public Guid TransactionId { get; set; }
        public DateTime Horodatage { get; set; }
        public string Details { get; set; }
    }
}
