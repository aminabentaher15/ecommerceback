﻿using ECommerce.Models;

namespace ECommerce.DTOs
{
    public class LigneCommandeDto
    {
        public Guid CommandeProduitId { get; set; }
        public Guid CommandeId { get; set; }
        public Guid ProduitId { get; set; }
        public float quantite { get; set; }
        public string? LibelleCommande { get; set; }
        public string? NomProduit { get; set; }
        public string? NomClient { get; set; }
        public Commande? Commande { get; set; }
        public Produit? Produit { get; set; }
    }
}
