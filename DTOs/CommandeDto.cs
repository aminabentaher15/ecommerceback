﻿namespace ECommerce.DTOs
{
    public class CommandeDto
    {
        public Guid CommandeId { get; set; }
        public string? Libelle { get; set; }
        public DateTime DateCommande { get; set; }
        public string? NomClient { get; set; }
        public string? NomProduit { get; set;}
    }
}
