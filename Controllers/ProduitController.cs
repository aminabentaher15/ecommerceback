﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ECommerce.Models;
using ECommerce;
using System;
using ECommerce.DTOs;
using TraceServer.Domain.Models;
namespace ECommerce.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProduitController : ControllerBase
    {
       private readonly ECommerceDbContext _context;
    private readonly TransactionService _transactionService;

    public ProduitController(ECommerceDbContext context, TransactionService transactionService)
    {
        _context = context;
        _transactionService = transactionService;
    }

        [HttpGet("getProduits")]
        public IEnumerable<Produit> GetProduits()
        {
            var produits = _context.Produits
                .Include(cp => cp.CommandeProduits)
                .ToList();
            return produits;
        }

        [HttpGet("getProduitById/{productId}")]
        public async Task<ActionResult<Produit>> GetProduitById(Guid productId)
        {
            var produit = await _context.Produits.FindAsync(productId);
            if (produit == null)
            {
                return NotFound();
            }
            return produit;
        }
        [HttpPost("addProduit")]
        public async Task<ActionResult<Produit>> AddProduit(Produit produit)
        {
            _context.Produits.Add(produit);
            await _context.SaveChangesAsync();
            var ApplicationId = new Guid("07CC6F97-21F1-4BFB-A59C-2AC3EB194F3A");

            var nouvelleTransaction = new Transaction
            {
                TransactionId = Guid.NewGuid(),
                Horodatage = DateTime.Now,
                Details = $"Ajout d'un nouveau produit avec nom : {produit.Nom}",
                Type = HttpContext.Request.Method,
                ApplicationId = ApplicationId
            };


            // Appel du service TransactionService pour enregistrer la nouvelle transaction
            await _transactionService.AddTransaction(nouvelleTransaction);

            return Ok(produit);
        }
    

    [HttpPut("updateProduit")]
        public async Task<ActionResult<Produit>> UpdateVendeur(Produit produit)
        {
            _context.Produits.Update(produit);
            await _context.SaveChangesAsync();
            var nouvelleTransaction = new Transaction
            {
                TransactionId = Guid.NewGuid(),
                Horodatage = DateTime.Now,
                Details = $"Modifier un produit avec nom : {produit.Nom}",
                Type = HttpContext.Request.Method,
                ApplicationId = new Guid("07CC6F97-21F1-4BFB-A59C-2AC3EB194F3A")
        };


            // Appel du service TransactionService pour enregistrer la nouvelle transaction
            await _transactionService.AddTransaction(nouvelleTransaction);
            return Ok(produit);
        }

        [HttpDelete("deleteProduit/{produitId}")]
        public async Task<IActionResult> DeleteProduit(Guid produitId)
        {
            var produit = await _context.Produits.FindAsync(produitId);
            if (produit == null)
            {
                return NotFound();
            }

            _context.Produits.Remove(produit);
            await _context.SaveChangesAsync();
            var nouvelleTransaction = new Transaction
            {
                TransactionId = Guid.NewGuid(),
                Horodatage = DateTime.Now,
                Details = $"Suppression d'un produit avec nom : {produit.Nom}",
                Type = HttpContext.Request.Method,
                ApplicationId = new Guid("07CC6F97-21F1-4BFB-A59C-2AC3EB194F3A")
        };


            // Appel du service TransactionService pour enregistrer la nouvelle transaction
            await _transactionService.AddTransaction(nouvelleTransaction);

            return Ok("Produit supprimé");
        }


           

    private bool ProduitExists(Guid id)
        {
            return _context.Produits.Any(e => e.ProduitId == id);
        }
    }
}
