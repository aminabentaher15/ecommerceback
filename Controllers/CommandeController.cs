﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ECommerce.Models;
using ECommerce;
using ECommerce.DTOs;
using AutoMapper;

namespace ECommerce.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommandeController : ControllerBase
    {
        private readonly ECommerceDbContext _context;
        private readonly IMapper _mapper;

        public CommandeController(ECommerceDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        [HttpGet("getCommandesDto")]
        public IEnumerable<CommandeDto> GetCommandesDto()
        {
            var commandes = _context.Commandes
                .Include(c => c.Client)
                .Include(c => c.CommandeProduits)
                .ThenInclude(cp => cp.Produit)
                .ToList();

            return _mapper.Map<IEnumerable<CommandeDto>>(commandes);
        }

        [HttpGet("getCommandeDtoById/{commandeId}")]
        public async Task<ActionResult<CommandeDto>> GetCommandeDtoById(Guid commandeId)
        {
            var commande = await _context.Commandes.FindAsync(commandeId);
            if (commande == null)
            {
                return NotFound();
            }

            var commandeDto = _mapper.Map<CommandeDto>(commande);
            return commandeDto;
        }


        [HttpGet("getCommandes")]
        public IEnumerable<Commande> GetCommandes()
        {
            var commandes = _context.Commandes
                .Include(c => c.Client)
                .Include(c => c.CommandeProduits)
                .ToList();
            return commandes;
        }

        [HttpGet("getCommandeById/{commandeId}")]
        public async Task<ActionResult<Commande>> GetCommandeById(Guid commandeId)
        {
            var commande = await _context.Commandes.FindAsync(commandeId);
            if (commande == null)
            {
                return NotFound();
            }
            return commande;
        }

        [HttpPost("addCommande")]
        public async Task<ActionResult<Commande>> AddCommande(Commande commande)
        {
            _context.Commandes.Add(commande);
            await _context.SaveChangesAsync();
            return Ok(commande);
        }

        [HttpPut("updateCommande/{commandeId}")]
        public async Task<IActionResult> UpdateCommande(Guid commandeId, Commande commande)
        {
            if (commandeId != commande.CommandeId)
            {
                return BadRequest();
            }

            _context.Entry(commande).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CommandeExists(commandeId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [HttpDelete("deleteCommande/{commandeId}")]
        public async Task<IActionResult> DeleteCommande(Guid commandeId)
        {
            var commande = await _context.Commandes.FindAsync(commandeId);
            if (commande == null)
            {
                return NotFound();
            }

            _context.Commandes.Remove(commande);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool CommandeExists(Guid id)
        {
            return _context.Commandes.Any(e => e.CommandeId == id);
        }

    }
}
