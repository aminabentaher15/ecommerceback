﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ECommerce.Models;
using ECommerce;

namespace ECommerce.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientController : ControllerBase
    {
        private readonly ECommerceDbContext _context;

        public ClientController(ECommerceDbContext context)
        {
            _context = context;
        }

        [HttpGet("getClients")]
        public IEnumerable<Client> GetClients()
        {
            var clients = _context.Clients
                .Include(cp => cp.Commandes)
                .ToList();
            return clients;
        }

        [HttpGet("getClientById/{clientId}")]
        public async Task<ActionResult<Client>> GetClientById(Guid clientId)
        {
            var client = await _context.Clients.FindAsync(clientId);
            if (client == null)
            {
                return NotFound();
            }
            return client;
        }

        [HttpPost("addClient")]
        public async Task<ActionResult<Client>> AddClient(Client client)
        {
            _context.Clients.Add(client);
            await _context.SaveChangesAsync();
            return Ok(client);
        }

        [HttpPut("updateClient")]
        public async Task<ActionResult<Client>> UpdateVendeur(Client client)
        {
            _context.Clients.Update(client);
            await _context.SaveChangesAsync();
            return Ok(client);
        }

        [HttpDelete("deleteClient/{clientId}")]
        public async Task<IActionResult> DeleteClient(Guid clientId)
        {
            var client = await _context.Clients.FindAsync(clientId);
            if (client == null)
            {
                return NotFound();
            }

            _context.Clients.Remove(client);
            await _context.SaveChangesAsync();

            return Ok("Client supprimé");
        }

        private bool ClientExists(Guid id)
        {
            return _context.Clients.Any(e => e.ClientId == id);
        }
    }
}
