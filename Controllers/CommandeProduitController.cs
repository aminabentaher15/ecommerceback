﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ECommerce.Models;
using ECommerce;
using AutoMapper;
using ECommerce.DTOs;

namespace ECommerce.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommandeProduitController : ControllerBase
    {
        private readonly ECommerceDbContext _context;
        private readonly IMapper _mapper; // Injectez IMapper ici

        public CommandeProduitController(ECommerceDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper; // Initialisez IMapper dans le constructeur
        }

        [HttpGet("getCommandeProduitsDto")]
        public IEnumerable<LigneCommandeDto> AfficheCommandeProduits()
        {
            var commandeProduits = _context.CommandeProduits
                .Include(cp => cp.Produit)
                .Include(cp => cp.Commande)
                   .ThenInclude(c => c.Client)
                .ToList();

            // Mapper CommandeProduit vers LigneCommandeDto
            var ligneCommandesDto = _mapper.Map<List<LigneCommandeDto>>(commandeProduits);

            return ligneCommandesDto;
        }

        [HttpGet("getCommandeProduits")]
        public IEnumerable<CommandeProduit> GetCommandeProduits()
        {
            var commandeProduits = _context.CommandeProduits
               .Include(cp => cp.Produit)
               .Include(cp => cp.Commande)
                   .ThenInclude(c => c.Client)
               .ToList();
            return commandeProduits;
        }

        [HttpGet("getCommandeProduitById/{commandeProduitId}")]
        public async Task<ActionResult<CommandeProduit>> GetCommandeProduitById(Guid commandeProduitId)
        {
            var commandeProduit = await _context.CommandeProduits.FindAsync(commandeProduitId);
            if (commandeProduit == null)
            {
                return NotFound();
            }
            return commandeProduit;
        }

        [HttpPost("addCommandeProduit")]
        public async Task<ActionResult<CommandeProduit>> AddCommandeProduit(CommandeProduit commandeProduit)
        {
            _context.CommandeProduits.Add(commandeProduit);
            await _context.SaveChangesAsync();
            return Ok(commandeProduit);
        }

        [HttpPut("updateCommandeProduit/{commandeProduitId}")]
        public async Task<IActionResult> UpdateCommandeProduit(Guid commandeProduitId, CommandeProduit commandeProduit)
        {
            if (commandeProduitId != commandeProduit.CommandeProduitId)
            {
                return BadRequest();
            }

            _context.Entry(commandeProduit).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CommandeProduitExists(commandeProduitId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [HttpDelete("deleteCommandeProduit/{commandeProduitId}")]
        public async Task<IActionResult> DeleteCommandeProduit(Guid commandeProduitId)
        {
            var commandeProduit = await _context.CommandeProduits.FindAsync(commandeProduitId);
            if (commandeProduit == null)
            {
                return NotFound();
            }

            _context.CommandeProduits.Remove(commandeProduit);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool CommandeProduitExists(Guid id)
        {
            return _context.CommandeProduits.Any(e => e.CommandeProduitId == id);
        }
    }
}
