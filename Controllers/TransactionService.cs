﻿namespace ECommerce.Controllers
{
    using System;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;
    using Newtonsoft.Json;
    using TraceServer.Domain.Models;
    public class TransactionService
    {
        private readonly HttpClient _httpClient;

        public TransactionService(HttpClient httpClient)
        {
            _httpClient = httpClient;
            _httpClient.BaseAddress = new Uri("https://localhost:7084"); 
        }

        public async Task AddTransaction(Transaction transaction)
        {
            var json = JsonConvert.SerializeObject(transaction);
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            var response = await _httpClient.PostAsync("/api/Transaction/AddTransaction", content);
            response.EnsureSuccessStatusCode(); // Gérer les erreurs selon vos besoins
        }
    }

}
